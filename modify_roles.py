from app import db
from app.models import User, Role

users = User.query.all()
for u in users:
    if u.role is None:
            u.role = Role.query.filter_by(default=True).first()
            db.session.add(u)
            db.session.commit()

