from flask import abort, flash

from flask.ext.restful import Resource, reqparse
from flask.ext.login import login_required

from app import db, api

from .models import Book, Author, Permission
from .functions import permission_required


class BookAPI(Resource):

    def __init__(self):
        self.book_parser = reqparse.RequestParser()
        self.book_parser.add_argument('id', type=int)
        self.book_parser.add_argument('title', type=str, required=True)
        self.book_parser.add_argument('authors')
        super(BookAPI, self).__init__()

    def get(self, id=None):
        # Return book data
        if not id:
            books = Book.query.all()
        else:
            books = [Book.query.get(id)]
        if not books or not books[0]:
            abort(404)
        res = {'books': []}
        book_dict = dict()
        for book in books:
            book_dict = {'id':book.id,
                         'title': book.title,
                         'authors': [a.name for a in book.authors]
                         }
            res['books'].append(book_dict)
        print("Res: {}".format(res))
        return res

    @login_required
    @permission_required(Permission.MODIFY_BOOKS)
    def post(self):
        # Create a new book
        args = self.book_parser.parse_args()
        title = args['title'].strip(' ')
        if args['authors']:
            authors = [author.strip(' ') for author in args['authors'].split(',')]
            book_auth = Author.query.filter(Author.name.in_(tuple(authors))).all()
        else:
            book_auth = []
        book = Book(title=title, authors=book_auth)
        db.session.add(book)
        db.session.commit()
        res = dict()
        authors_list = [a.name for a in book_auth]
        res[book.id] = {
            'title': book.title,
            'authors': authors_list
        }
        return res

    @login_required
    @permission_required(Permission.MODIFY_BOOKS)
    def put(self, id):
        # Update the book with given id
        args = self.book_parser.parse_args()
        title = args['title']
        if args['authors']:
            authors = [author.strip(' ') for author in args['authors'].split(',')]
            print("INFO authors: {}".format(authors))
            book_auth = Author.query.filter(Author.name.in_(tuple(authors))).all()
        else:
            book_auth = []
        print("INFO book_auth: {}".format(book_auth))
        book = Book.query.get(id)
        book.title = title.strip(' ')
        book.authors = book_auth
        db.session.commit()
        book = Book.query.get_or_404(id)
        res = dict()
        authors_list = [a.name for a in book_auth]
        res[book.id] = {
            'title': book.title,
            'authors': authors_list
        }
        return res

    @login_required
    @permission_required(Permission.MODIFY_BOOKS)
    def delete(self, id):
        # Delete the book with given id
        book = Book.query.get(id)
        db.session.delete(book)
        db.session.commit()
        return {'status':'Success'}


class AuthorAPI(Resource):

    def __init__(self):
        self.author_parser = reqparse.RequestParser()
        self.author_parser.add_argument('id', type=int)
        self.author_parser.add_argument('name', type=str, required=True)
        super(AuthorAPI, self).__init__()

    def get(self, id=None):
        # Return author data
        if not id:
            authors = Author.query.all()
        else:
            authors = [Author.query.get(id)]
        if not authors or not authors[0]:
            abort(404)
        res = {'authors':[]}
        for author in authors:
            author_dict = {
                'id': author.id,
                'name': author.name,
                'books': [book.title for book in author.books]
            }
            res['authors'].append(author_dict)
        return res

    @login_required
    @permission_required(Permission.MODIFY_AUTHORS)
    def post(self):
        # Create a new author
        args = self.author_parser.parse_args()
        name = args['name'].strip(' ')
        author = Author(name=name)
        db.session.add(author)
        db.session.commit()
        res = {'authors':[]}
        author_dict = {
            'id': author.id,
            'name': author.name,
            'books': [book.title for book in author.books]
        }
        res['authors'].append(author_dict)
        return res

    @login_required
    @permission_required(Permission.MODIFY_AUTHORS)
    def put(self, id):
        # Update the author with given id
        args = self.author_parser.parse_args()
        name = args['name'].strip(' ')
        author = Author.query.get(id)
        author.name = name
        db.session.commit()
        author = Author.query.get_or_404(id)
        res = {'authors':[]}
        author_dict = {
            'id': author.id,
            'name': author.name,
            'books': [book.title for book in author.books]
        }
        res['authors'].append(author_dict)
        return res

    @login_required
    @permission_required(Permission.MODIFY_AUTHORS)
    def delete(self, id):
        # Delete the author with given id
        author = Author.query.get(id)
        db.session.delete(author)
        db.session.commit()
        return {'status':'Success'}