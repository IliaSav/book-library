from flask import render_template, flash, redirect, url_for, request
from flask.ext.login import login_required, login_user, logout_user, current_user
from flask.ext.mail import Message

from app import app, db, mail, api, manager_api

from .forms import LoginForm, RegistrationForm
from .models import User, Book, Author
from . import login_manager
from .library_api import BookAPI, AuthorAPI


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


def send_email(to, subject, template, **kwargs):
    msg = Message(app.config['MAIL_SUBJECT_PREFIX'] + subject,
                  sender=app.config['MAIL_SENDER'], recipients=[to])
    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)
    mail.send(msg)


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template("index.html",
                           title='Home')


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('index'))
        flash('Invalid username or password.')
    return render_template('login.html', form=form)


@app.route('/logout')
def logout():
    logout_user()
    flash('You have been logged out.')
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(nickname=form.username.data,
                    email=form.email.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        token = user.generate_confirmation_token()
        send_email(user.email, 'Confirm Your Account', 'email', user=user, token=token)
        flash('A confirmation email has been sent to you by email.')
        return redirect(url_for('index'))
    return render_template('register.html', form=form)


@app.route('/confirm/<token>')
@login_required
def confirm(token):
    if current_user.confirmed:
        return redirect(url_for('index'))
    if current_user.confirm(token):
        flash('You have confirmed your account. Thanks!')
    else:
        flash('The confirmation link is invalid or has expired.')
    return redirect(url_for('index'))


@app.before_first_request
def before_request():
    if current_user.is_authenticated and not current_user.confirmed:
        return redirect(url_for('unconfirmed'))


@app.route('/unconfirmed')
def unconfirmed():
    if current_user.is_anonymous() or current_user.confirmed():
        return redirect('index')
    return redirect('confirm')


@app.route('/confirm')
@login_required
def resend_confirmation():
    token = current_user.generate_confirmation_token()
    send_email('email', 'Confirm Your Account', user, token=token)
    flash('A new confirmation email has been sent to you by email.')
    return redirect(url_for('index'))

api.add_resource(
    BookAPI,
    '/api/book',
    '/api/book/<int:id>'
)

api.add_resource(
    AuthorAPI,
    '/api/author',
    '/api/author/<int:id>'
)

# manager_api.create_api(Book, methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])
# manager_api.create_api(Author, methods=['GET', 'POST', 'PUT', 'PATCH', 'DELETE'])


