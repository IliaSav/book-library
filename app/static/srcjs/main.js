var React = require('react');
var components = require('./components');
var components_author = require('./components_author');
var stores = require('./store');
var store_authors = require('./store_authors');

React.render(<components.BookPanel url='/api/book' />, document.getElementById('book'));
React.render(<components_author.AuthorPanel url='/api/author' />, document.getElementById('author'));


stores.reloadBooks();
store_authors.reloadAuthors();