/**
 * Created by admin on 09-Mar-16.
 */
var $ = require('jquery');
var _ = require('underscore');
var AppDispatcher = require('./actions').AppDispatcher;
var AuthorConstants = require('./constants');
var stores = require('./store');


var _state_author = {
    authors: [],
    message:"",
    editingAuthor: null
};

var _props_author = {
    url: '/api/author',
    authors: []
};

var _search = function(query) {
    _state_author.authors = _.filter(_props_author.authors, function(obj) {
        return ~obj.name.toLowerCase().indexOf(query);
    });
    AuthorStore.emitChange();
};


var _reloadAuthors = function() {
    $.ajax({
        url: _props_author.url,
        dataType: 'json',
        cache: false,
        success: function(data) {
            console.log("React author:"+JSON.stringify(data));
            //_props.books = data['objects'];
            //_state.books = data['objects'];
            _props_author.authors = data['authors'];
            _state_author.authors = data['authors'];
            AuthorStore.emitChange();
            stores.reloadBooks();
        },
        error: function(xhr, status, err) {
            console.error(this.url, status, err.toString());
            _state_author.message = err.toString();
            _props_author.authors = [];
            _state_author.authors = [];
            AuthorStore.emitChange();
        }
    });
};

var _saveAuthor = function(author) {
    if(author.id) {
        $.ajax({
            url: _props_author.url+'/'+author.id,
            dataType: 'json',
            headers:{
                'Content-Type': 'application/json'
            },
            method: 'PUT',
            data:JSON.stringify(author),
            cache: false,
            success: function(data) {
                _state_author.message = "Successfully updated author!";
                _clearEditingAuthor();
                _reloadAuthors();
            },
            error: function(xhr, status, err) {
                _state_author.message = "You can't update author.";//+err.toString();
                AuthorStore.emitChange();
            }
        });
    } else {
        $.ajax({
            url: _props_author.url,
            dataType: 'json',
            headers:{
                'Content-Type': 'application/json'
            },
            method: 'POST',
            data:JSON.stringify(author),
            cache: false,
            success: function(data) {
                _state_author.message = "Successfully added author!";
                _clearEditingAuthor();
                _reloadAuthors();
            },
            error: function(xhr, status, err) {
                _state_author.message = "You can't add author.";//+err.toString();
                AuthorStore.emitChange();
            }
        });
    }
};

var _deleteAuthor = function(authorId) {
    $.ajax({
        url: _props_author.url+'/'+authorId,
        method: 'DELETE',
        cache: false,
        success: function(data) {
            _state_author.message = "Successfully deleted author!";
            _clearEditingAuthor();
            _reloadAuthors();
        },
        error: function(xhr, status, err) {
            console.error(this.props.url, status, err.toString());
            _state_author.message = "You can't delete author.";//+err.toString();
            AuthorStore.emitChange();
        }
    });
};

var _clearEditingAuthor = function() {
    _state_author.editingAuthor = null;
};

var _editAuthor = function(author) {
    _state_author.editingAuthor = author;
    AuthorStore.emitChange();
};

var _cancelEditAuthor = function() {
    _clearEditingAuthor();
    AuthorStore.emitChange();
};


var AuthorStore = {
    listeners: [],
    getState: function() {
        return _state_author;
    },
    emitChange: function() {
        var i;
        for(i=0;i<this.listeners.length;i++) {
            this.listeners[i]();
        }
    },
    addChangeListener: function(callback) {
        this.listeners.push(callback);
    },
    removeChangeListener: function(callback) {
        this.listeners.splice(this.listeners.indexOf(callback), 1);
    }
};

AppDispatcher.register(function(action) {
    switch(action.actionType) {
        case AuthorConstants.AUTHOR_EDIT:
            _editAuthor(action.author);
        break;
        case AuthorConstants.AUTHOR_EDIT_CANCEL:
            _cancelEditAuthor();
        break;
        case AuthorConstants.AUTHOR_SAVE:
            _saveAuthor(action.author);
        break;
        case AuthorConstants.AUTHOR_SEARCH:
            _search(action.query);
        break;
        case AuthorConstants.AUTHOR_DELETE:
            _deleteAuthor(action.authorId);
        break;
    }
    return true;
});

module.exports.AuthorStore = AuthorStore;
module.exports.reloadAuthors = _reloadAuthors;