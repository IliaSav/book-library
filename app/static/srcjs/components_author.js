var React = require('react');
var AuthorStore = require('./store_authors').AuthorStore;
var AuthorActions = require('./actions').AuthorActions;

var AuthorTableRow = React.createClass({
    render: function() {
        return (
            <tr>
                <td>{this.props.author.id}</td>
                <td>{this.props.author.name}</td>
                <td><a href='#' onClick={this.onClick}>Edit</a></td>
            </tr>
        );
    },
    onClick: function(e) {
        e.preventDefault();
        AuthorActions.edit(this.props.author);
    }
});

var AuthorTable = React.createClass({
    render: function() {
        var rows = [];
        this.props.authors.forEach(function(author) {
            rows.push(<AuthorTableRow key={author.id} author={author} />);
        });
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
});

var AuthorForm = React.createClass({
    getInitialState: function() {
        if (this.props.author) {
            return this.props.author;
        } else {
            return {};
        }
    },
    componentWillReceiveProps: function(props) {
        if (props.author) {
            this.setState(props.author);
        } else {
            this.replaceState({});
        }
    },
    render: function() {
        return(
            <form
                onSubmit={this.onSubmit}
                className="form-inline">
                <div className="form-group">
                    <label forHtml='name'>Name</label>
                    <input
                        ref='name'
                        name='name'
                        className="form-control"
                        type='text'
                        required={true}
                        placeholder='author1'
                        value={this.state.name}
                        onChange={this.onFormChange} />
                </div>
                <div className="btn-group form-group" role="group" aria-label="...">
                    <button
                        type='submit'
                        className="btn btn-default"
                        >
                        {this.state.id?
                            <span className="glyphicon glyphicon-floppy-disk"
                                  aria-hidden="true">
                            </span>:
                            <span
                                className="glyphicon glyphicon-plus"
                                aria-hidden="true">
                            </span>
                        }
                    </button>
                    {this.state.id?
                    <button
                        onClick={this.onDeleteClick}
                        className="btn btn-default">
                        <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>:""}
                {this.state.id?
                    <button
                        onClick={this.onCancelClick}
                        className="btn btn-default">
                        <span className="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                    </button>:""}
                    </div>
                {this.props.message?<div>{this.props.message}</div>:""}
            </form>
        );
    },
    onFormChange: function() {
        this.setState({
            name: React.findDOMNode(this.refs.name).value
        })
    },
    onSubmit: function(e) {
        e.preventDefault();
        AuthorActions.save(this.state)
    },
    onCancelClick: function(e) {
        e.preventDefault();
        AuthorActions.edit_cancel()
    },
    onDeleteClick: function(e) {
        e.preventDefault();
        AuthorActions.delete(this.state.id)
    }
});

var SearchPanel = React.createClass({
    getInitialState: function() {
        return {
            search: ''
        }
    },
    render: function() {
        return (
            <form className="form-inline">
                <div className="form-group">
                    <label forHtml='search'>Search: </label>
                    <input
                        ref='search'
                        name='search'
                        className="form-control"
                        type='text'
                        value={this.state.search}
                        onChange={this.onSearchChange} />
                    {this.state.search?
                        <button
                            onClick={this.onClearSearch}
                            className="btn btn-default" >
                            <span
                                className="glyphicon glyphicon-remove"
                                aria-hidden="true">
                            </span>
                        </button>:
                        ''
                    }
                </div>
            </form>
        )
    },
    onSearchChange: function() {
        var query = React.findDOMNode(this.refs.search).value;
        if (this.promise) {
            clearInterval(this.promise)
        }
        this.setState({
            search: query
        });
        this.promise = setTimeout(function () {
            AuthorActions.search(query);
        }.bind(this), 200);
    },
    onClearSearch: function() {
        this.setState({
            search: ''
        });
        AuthorActions.search('');
    }
});

var AuthorPanel = React.createClass({
    getInitialState: function() {
        return AuthorStore.getState();
    },
    render: function() {
        return(
            <div className="row">
                <h3>Authors</h3>
                <div>
                    <AuthorForm
                        author={this.state.editingAuthor}
                        message={this.state.message}
                    />
                </div>
                <br />
                <div>
                    <SearchPanel></SearchPanel>
                    <AuthorTable authors={this.state.authors} />
                </div>
                <br />
            </div>
        );
    },
    _onChange: function() {
        this.setState( AuthorStore.getState() );
    },
    componentWillUnmount: function() {
        AuthorStore.removeChangeListener(this._onChange);
    },
    componentDidMount: function() {
        AuthorStore.addChangeListener(this._onChange);
    }
});

module.exports.AuthorPanel = AuthorPanel ;