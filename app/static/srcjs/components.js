//import React from 'react'
//import {BookStore} from './store'
//import {BookActions} from './actions'
var React = require('react');
var BookStore = require('./store').BookStore;
var BookActions = require('./actions').BookActions;

var BookTableRow = React.createClass({
    render: function() {
        return (
            <tr>
                <td>{this.props.book.id}</td>
                <td>{this.props.book.title}</td>
                <td>{this.props.book.authors.toString()}</td>
                <td><a href='#' onClick={this.onClick}>Edit</a></td>
            </tr>
        );
    },
    onClick: function(e) {
        e.preventDefault();
        BookActions.edit(this.props.book);
    }
});

var BookTable = React.createClass({
    render: function() {
        var rows = [];
        this.props.books.forEach(function(book) {
            rows.push(<BookTableRow key={book.id} book={book} />);
        });
        return (
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Title</th>
                        <th>Authors</th>
                        <th>Edit</th>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
});

var BookForm = React.createClass({
    getInitialState: function() {
        if (this.props.book) {
            return this.props.book;
        } else {
            return {};
        }
    },
    componentWillReceiveProps: function(props) {
        if (props.book) {
            this.setState(props.book);
        } else {
            this.replaceState({});
        }
    },
    render: function() {
        return(
            <form
                onSubmit={this.onSubmit}
                className="form-inline"
                >
                <div className="form-group">
                    <label forHtml='title'>Title</label>
                    <input
                        ref='title'
                        name='title'
                        className="form-control"
                        type='text'
                        required={true}
                        placeholder='title1'
                        value={this.state.title}
                        onChange={this.onFormChange} />
                </div>
                <div className="form-group">
                    <label forHtml='authors'>Authors</label>
                    <input
                        ref='authors'
                        name='authors'
                        className="form-control"
                        type='text'
                        placeholder='name1,name2'
                        value={this.state.authors}
                        onChange={this.onFormChange} />
                </div>
                <div className="btn-group form-group" role="group" aria-label="...">
                <button
                    type='submit'
                    className="btn btn-default"
                    >
                    {this.state.id?
                        <span className="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>:
                        <span className="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    }
                </button>
                {this.state.id?
                    <button
                        onClick={this.onDeleteClick}
                        className="btn btn-default">
                        <span className="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>:""}
                {this.state.id?
                    <button
                        onClick={this.onCancelClick}
                        className="btn btn-default">
                        <span className="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                    </button>:""}
                    </div>
                {this.props.message?<div>{this.props.message}</div>:""}
            </form>
        );
    },
    onFormChange: function() {
        this.setState({
            title: React.findDOMNode(this.refs.title).value,
            authors: React.findDOMNode(this.refs.authors).value
        })
    },
    onSubmit: function(e) {
        e.preventDefault();
        BookActions.save(this.state)
    },
    onCancelClick: function(e) {
        e.preventDefault();
        BookActions.edit_cancel()
    },
    onDeleteClick: function(e) {
        e.preventDefault();
        BookActions.delete(this.state.id)
    }
});

var SearchPanel = React.createClass({
    getInitialState: function() {
        return {
            search: ''
        }
    },
    render: function() {
        return (
            <form className="form-inline">
                <div className="form-group">
                    <label forHtml='search'>Search:</label>
                    <input ref='search' name='search' className="form-control" type='text' value={this.state.search} onChange={this.onSearchChange} />
                    {this.state.search?
                        <button
                            onClick={this.onClearSearch}
                            className="btn btn-default" >
                            <span
                                className="glyphicon glyphicon-remove"
                                aria-hidden="true">
                            </span>
                        </button>:''
                    }
                </div>
            </form>
        )
    },
    onSearchChange: function() {
        var query = React.findDOMNode(this.refs.search).value;
        if (this.promise) {
            clearInterval(this.promise)
        }
        this.setState({
            search: query
        });
        this.promise = setTimeout(function () {
            BookActions.search(query);
        }.bind(this), 200);
    },
    onClearSearch: function() {
        this.setState({
            search: ''
        });
        BookActions.search('');
    }
});

var BookPanel = React.createClass({
    getInitialState: function() {
        return BookStore.getState();
    },
    render: function() {
        return(
            <div className="row">
                <h3>Books</h3>
                <div>
                    <BookForm
                        book={this.state.editingBook}
                        message={this.state.message}
                    />
                </div>
                <br />
                <div>
                    <SearchPanel></SearchPanel>
                    <BookTable books={this.state.books} />
                </div>
            </div>
        );
    },
    _onChange: function() {
        this.setState( BookStore.getState() );
    },
    componentWillUnmount: function() {
        BookStore.removeChangeListener(this._onChange);
    },
    componentDidMount: function() {
        BookStore.addChangeListener(this._onChange);
    }
});

module.exports.BookPanel = BookPanel ;