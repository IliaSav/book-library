//import {BookConstants} from './constants'
//import {Dispatcher} from 'flux'
//var AppDispatcher = new Dispatcher();

var Constants = require('./constants');
var Dispatcher = require('flux').Dispatcher;
var AppDispatcher = new Dispatcher();

var BookActions = {
    search: function(query) {
        AppDispatcher.dispatch({
            actionType: Constants.BOOK_SEARCH,
            query: query
        });
    },
    save: function(book) {
        AppDispatcher.dispatch({
            actionType: Constants.BOOK_SAVE,
            book: book
        });
    },
    edit: function(book) {
        AppDispatcher.dispatch({
            actionType: Constants.BOOK_EDIT,
            book: book
        });
    },
    edit_cancel: function() {
        AppDispatcher.dispatch({
            actionType: Constants.BOOK_EDIT_CANCEL
        });
    },
    delete: function(bookId) {
        AppDispatcher.dispatch({
            actionType: Constants.BOOK_DELETE,
            bookId: bookId
        });
    }
};

var AuthorActions = {
    search: function(query) {
        AppDispatcher.dispatch({
            actionType: Constants.AUTHOR_SEARCH,
            query: query
        });
    },
    save: function(author) {
        AppDispatcher.dispatch({
            actionType: Constants.AUTHOR_SAVE,
            author: author
        });
    },
    edit: function(author) {
        AppDispatcher.dispatch({
            actionType: Constants.AUTHOR_EDIT,
            author: author
        });
    },
    edit_cancel: function() {
        AppDispatcher.dispatch({
            actionType: Constants.AUTHOR_EDIT_CANCEL
        });
    },
    delete: function(authorId) {
        AppDispatcher.dispatch({
            actionType: Constants.AUTHOR_DELETE,
            authorId: authorId
        });
    }
};

module.exports.BookActions = BookActions;
module.exports.AuthorActions = AuthorActions;
module.exports.AppDispatcher = AppDispatcher;