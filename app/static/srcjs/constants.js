module.exports = {
    BOOK_EDIT: 'BOOK_EDIT',
    BOOK_EDIT_CANCEL: 'BOOK_EDIT_CANCEL',
    BOOK_SAVE: 'BOOK_SAVE',
    BOOK_SEARCH: 'BOOK_SEARCH',
    BOOK_DELETE: 'BOOK_DELETE',
    AUTHOR_EDIT: 'AUTHOR_EDIT',
    AUTHOR_EDIT_CANCEL: 'AUTHOR_EDIT_CANCEL',
    AUTHOR_SAVE: 'AUTHOR_SAVE',
    AUTHOR_SEARCH: 'AUTHOR_SEARCH',
    AUTHOR_DELETE: 'AUTHOR_DELETE'
};