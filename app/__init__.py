from flask import Flask

from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from flask.ext.mail import Mail
from flask.ext.moment import Moment
from flask.ext.restful import Api
from flask.ext.restless import APIManager

from config import Config, HerokuConfig

app = Flask(__name__)
# app.config.from_object(Config)
app.config.from_object(HerokuConfig)
db = SQLAlchemy(app)
manager_api = APIManager(app, flask_sqlalchemy_db=db)
migrate = Migrate(app, db)

api = Api(app)
api.init_app(app)

bootstrap = Bootstrap()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'login'

login_manager.init_app(app)
bootstrap.init_app(app)

mail = Mail(app)
moment = Moment(app)


from app import views, models

# locale
# db.create_all()
